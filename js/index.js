
let pages = [
	['home.html', 'Home' ,'fas fa-home'],
	['', 'Gallery' ,'fas fa-images'],
	['', 'About' ,'fas fa-book'],
];

$(document).ready(function () {
	$('#btn_menu').click(function () {
		let side_bar = $('.side-bar').eq(0);
		let side_bar_item_name = $('.side-bar-menu-item p');
		let side_bar_header_desc = $('.side-bar-header p');
		let side_bar_title = $('.side-bar-title');
		let side_bar_footer = $('.side-bar-footer');
		if(!side_bar.hasClass('active')){
			side_bar.width(50);
			side_bar_item_name.hide();
			side_bar_header_desc.hide();
			side_bar_title.hide();
			side_bar_footer.hide();
		} else {
			side_bar.width(250);
			side_bar_item_name.show();
			side_bar_header_desc.show();
			side_bar_title.show();
			side_bar_footer.show();
		}
		$(this).toggleClass('active');
		side_bar.toggleClass('active');
	});

	mountMenu();
});


function mountMenu() {
	let side_bar_menu = $('.side-bar-menu');

	side_bar_menu.html('');
	for(let i = 0; i < pages.length; i++){
		side_bar_menu.append(
			"<div class='side-bar-menu-item'>" +
				"<i class='" + pages[i][2] + "'></i>" +
				"<p>" + pages[i][1] + "</p>" +
			"</div>"
		);
	}

	$('.side-bar-menu-item').click(function () {
		$('.content-main-body').eq(0).load('pages/'+pages[$(this).index()][0]);
	});

}
